package com.example.abc.scribdassignment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.abc.scribdassignment.model.Forecast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class SearchAdapter extends ListAdapter<Forecast, SearchAdapter.SearchResultViewHolder> {

    SearchAdapter() {
        super(new DiffUtil.ItemCallback<Forecast>() {
            @Override
            public boolean areItemsTheSame(@NonNull Forecast oldItem, @NonNull Forecast newItem) {
                return oldItem == newItem;
            }

            @Override
            public boolean areContentsTheSame(@NonNull Forecast oldItem,
                @NonNull Forecast newItem) {
                return oldItem.equals(newItem);
            }
        });
    }

    @NonNull
    @Override
    public SearchResultViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_search_result,
            parent, false);
        return new SearchResultViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SearchResultViewHolder holder, int position) {
        holder.bindView(getItem(position));
    }

    class SearchResultViewHolder extends RecyclerView.ViewHolder {

        private TextView tvDate;
        private TextView tvDescription;
        private TextView tvMaxTemp;
        private TextView tvMinTemp;
        private TextView tvHumidity;
        private TextView tvWindSpeed;

        SearchResultViewHolder(@NonNull View itemView) {
            super(itemView);

            tvDate = itemView.findViewById(R.id.search_result_tv_date);
            tvDescription = itemView.findViewById(R.id.search_result_tv_description);
            tvMaxTemp = itemView.findViewById(R.id.search_result_tv_max_temp);
            tvMinTemp = itemView.findViewById(R.id.search_result_tv_min_temp);
            tvHumidity = itemView.findViewById(R.id.search_result_tv_humidity);
            tvWindSpeed = itemView.findViewById(R.id.search_result_tv_wind_speed);
        }

        void bindView(Forecast forecast) {
            Context context = itemView.getContext();
            Date date = new Date(1000L * forecast.date); // convert unix time to millis
            SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd, yyyy h:mm aaa", Locale.CANADA);
            tvDate.setText(sdf.format(date));

            if (forecast.weathers != null && !forecast.weathers.isEmpty()) {
                tvDescription.setText(forecast.weathers.get(0).description);
            }

            int maxTemp = (int)Math.round(forecast.data.tempMax);
            int minTemp = (int)Math.round(forecast.data.tempMin);
            tvMaxTemp.setText(context.getString(R.string.max_temp, maxTemp));
            tvMinTemp.setText(context.getString(R.string.min_temp, minTemp));
            tvHumidity.setText(context.getString(R.string.humidity, forecast.data.humidity));
            tvWindSpeed.setText(context.getString(R.string.wind_speed, forecast.wind.speed));
        }
    }
}
