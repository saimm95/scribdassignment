package com.example.abc.scribdassignment.network;

import com.example.abc.scribdassignment.model.WeatherApiResponse;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WeatherApi {

    @GET("/data/2.5/forecast")
    Single<WeatherApiResponse> getForecastData(@Query("APPID") String apiKey,
        @Query("units") String unit,
        @Query("q") String query);
}
