package com.example.abc.scribdassignment;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.abc.scribdassignment.common.Logger;
import com.example.abc.scribdassignment.common.SimpleVerticalDividerItemDecoration;
import com.example.abc.scribdassignment.model.Forecast;
import com.example.abc.scribdassignment.network.WeatherRepository;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements MainView {

    private TextView tvInstructions;
    private TextView tvError;
    private ProgressBar progressBar;
    private RecyclerView rvSearchResults;
    private SearchView searchView;

    private SearchAdapter searchAdapter;
    private MainPresenter presenter;
    private boolean showInstructions = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bindViews();
        initViews();

        presenter = new MainPresenter(new WeatherRepository());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onViewDestroyed();
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.bindView(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.unbindView();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleIntent(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.toolbar_menu, menu);

        SearchManager searchManager = (SearchManager)getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView)menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        searchView.addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {
            @Override
            public void onViewAttachedToWindow(View v) {
                tvInstructions.setVisibility(View.GONE);
            }

            @Override
            public void onViewDetachedFromWindow(View v) {
                tvInstructions.setVisibility(showInstructions ? View.VISIBLE : View.GONE);
            }
        });

        return true;
    }

    @Override
    public void showSearchResults(List<Forecast> forecasts) {
        tvError.setVisibility(View.GONE);
        rvSearchResults.setVisibility(View.VISIBLE);
        searchAdapter.submitList(new ArrayList<>(forecasts));
    }

    @Override
    public void showLoading(boolean loading) {
        progressBar.setVisibility(loading ? View.VISIBLE : View.GONE);

        if (loading) {
            rvSearchResults.setVisibility(View.GONE);
            tvError.setVisibility(View.GONE);
        }
    }

    @Override
    public void showError() {
        rvSearchResults.setVisibility(View.GONE);
        tvError.setVisibility(View.VISIBLE);
    }

    private void bindViews() {
        tvInstructions = findViewById(R.id.tv_instructions);
        tvError = findViewById(R.id.tv_error);
        progressBar = findViewById(R.id.progress_bar);
        rvSearchResults = findViewById(R.id.rv_search_results);
    }

    private void initViews() {
        searchAdapter = new SearchAdapter();
        rvSearchResults.setAdapter(searchAdapter);

        SimpleVerticalDividerItemDecoration itemDecoration =
            new SimpleVerticalDividerItemDecoration.Builder(this)
                .setLastItemHasDivider(false)
                .build();
        rvSearchResults.addItemDecoration(itemDecoration);
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            Logger.d(getClass(), "searched for:" + query);
            presenter.onSearch(query);
            searchView.clearFocus();
            showInstructions = false;
        }
    }
}
