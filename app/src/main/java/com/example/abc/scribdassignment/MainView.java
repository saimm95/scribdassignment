package com.example.abc.scribdassignment;

import com.example.abc.scribdassignment.model.Forecast;

import java.util.List;

public interface MainView {

    void showSearchResults(List<Forecast> forecasts);

    void showLoading(boolean loading);

    void showError();
}
