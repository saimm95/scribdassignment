package com.example.abc.scribdassignment.common;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.GradientDrawable;
import android.view.View;

import com.example.abc.scribdassignment.R;

import androidx.annotation.ColorRes;
import androidx.annotation.DimenRes;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

public final class SimpleVerticalDividerItemDecoration extends RecyclerView.ItemDecoration {

    private static final int DEFAULT_HEIGHT_RES = R.dimen.simple_vertical_divider_height;
    private static final int DEFAULT_COLOR_RES = R.color.simple_vertical_divider_color;
    private int height;
    private boolean lastItemHasDivider;
    private boolean firstItemHasTopDivider = false;
    private int leftMargin = 0;
    private int rightMargin = 0;
    private GradientDrawable divider = new GradientDrawable();

    public SimpleVerticalDividerItemDecoration(Context context) {
        this(context, DEFAULT_HEIGHT_RES, DEFAULT_COLOR_RES, true);
    }

    public SimpleVerticalDividerItemDecoration(Context context, boolean lastItemHasDivider) {
        this(context, DEFAULT_HEIGHT_RES, DEFAULT_COLOR_RES, lastItemHasDivider);
    }

    public SimpleVerticalDividerItemDecoration(Context context, @ColorRes int colorRes,
        boolean lastItemHasDivider) {
        this(context, DEFAULT_HEIGHT_RES, colorRes, lastItemHasDivider);
    }

    private SimpleVerticalDividerItemDecoration(Context context, @DimenRes int heightRes,
        @ColorRes int colorRes,
        boolean lastItemHasDivider) {
        this.height = context.getResources().getDimensionPixelOffset(heightRes);
        this.lastItemHasDivider = lastItemHasDivider;
        divider.setColor(ContextCompat.getColor(context, colorRes));
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view,
        @NonNull RecyclerView parent,
        @NonNull RecyclerView.State state) {
        if (parent.getAdapter() == null) {
            return;
        }

        boolean isFirstItem = parent.getChildAdapterPosition(view) == 0;
        boolean isLastItem =
            parent.getChildAdapterPosition(view) == parent.getAdapter().getItemCount() - 1;

        if (isFirstItem && firstItemHasTopDivider) {
            outRect.top = height;
        }

        if (!lastItemHasDivider && isLastItem) {
            return;
        }
        outRect.bottom = height;
    }

    @Override
    public void onDrawOver(@NonNull Canvas canvas, @NonNull RecyclerView parent,
        @NonNull RecyclerView.State state) {
        int left = leftMargin + parent.getLeft() + parent.getPaddingStart();
        int right = parent.getRight() - parent.getPaddingEnd() - rightMargin;

        int childCount = parent.getChildCount();
        int top, bottom;

        for (int i = 0; i < childCount; i++) {
            View child = parent.getChildAt(i);
            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams)child.getLayoutParams();

            // draw top divider for first item (if enabled)
            boolean isFirstItem = i == 0;
            if (isFirstItem && firstItemHasTopDivider) {
                top = child.getTop() + params.topMargin;
                bottom = top + height;

                divider.setBounds(left, top, right, bottom);
                divider.draw(canvas);
            }

            // draw bottom divider for all items (if applicable)
            boolean isLastItem = i == childCount - 1;
            if (!lastItemHasDivider && isLastItem) {
                return;
            }

            top = child.getBottom() + params.bottomMargin;
            bottom = top + height;

            divider.setBounds(left, top, right, bottom);
            divider.draw(canvas);
        }
    }

    public static class Builder {

        private SimpleVerticalDividerItemDecoration decoration;
        private Context context;

        public Builder(Context context) {
            decoration = new SimpleVerticalDividerItemDecoration(context);
            this.context = context;
        }

        public Builder setLastItemHasDivider(boolean lastItemHasDivider) {
            decoration.lastItemHasDivider = lastItemHasDivider;
            return this;
        }

        public Builder setHeightRes(@DimenRes int heightRes) {
            decoration.height = context.getResources().getDimensionPixelOffset(heightRes);
            return this;
        }

        public Builder setColorRes(@ColorRes int colorRes) {
            decoration.divider.setColor(ContextCompat.getColor(context, colorRes));
            return this;
        }

        public Builder setFirstItemHasTopDivider(boolean firstItemHasTopDivider) {
            decoration.firstItemHasTopDivider = firstItemHasTopDivider;
            return this;
        }

        public Builder setLeftMargin(@DimenRes int leftMarginRes) {
            decoration.leftMargin = context.getResources().getDimensionPixelOffset(leftMarginRes);
            return this;
        }

        public Builder setRightMargin(@DimenRes int rightMarginRes) {
            decoration.rightMargin = context.getResources().getDimensionPixelOffset(rightMarginRes);
            return this;
        }

        public SimpleVerticalDividerItemDecoration build() {
            return decoration;
        }
    }
}
