package com.example.abc.scribdassignment;

import com.example.abc.scribdassignment.common.Logger;
import com.example.abc.scribdassignment.network.WeatherRepository;

import androidx.annotation.Nullable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

class MainPresenter {

    private final WeatherRepository weatherRepository;
    @Nullable private MainView view;
    @Nullable private Disposable disposable;

    MainPresenter(WeatherRepository weatherRepository) {
        this.weatherRepository = weatherRepository;
    }

    void bindView(MainView view) {
        this.view = view;
    }

    void unbindView() {
        this.view = null;
    }

    void onViewDestroyed() {
        if (disposable != null) {
            disposable.dispose();
        }
    }

    void onSearch(String city) {
        executeViewOperation(() -> view.showLoading(true));
        disposable = weatherRepository.getForecasts(city)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doFinally(() -> executeViewOperation(() -> view.showLoading(false)))
            .subscribe(forecasts ->
                    executeViewOperation(() -> view.showSearchResults(forecasts)),
                throwable -> {
                    Logger.e(getClass(), throwable);
                    executeViewOperation(() -> view.showError());
                });
    }

    private void executeViewOperation(ViewOperation viewOperation) {
        if (view != null) {
            viewOperation.execute();
        }
    }

    interface ViewOperation {
        void execute();
    }
}
