package com.example.abc.scribdassignment.network;

import com.example.abc.scribdassignment.model.Forecast;

import io.reactivex.Single;

import java.util.List;

public class WeatherRepository {

    private static final String API_KEY = "eb94dd62294c94147bd1cb70148a8ece";
    private static final String UNIT = "metric";
    // Search Canadian cities only
    private static final String COUNTRY_CODE = "CA";

    private final WeatherApi weatherApi;

    public WeatherRepository() {
        this.weatherApi = RetrofitClient.getInstance().getRetrofit().create(WeatherApi.class);
    }

    public Single<List<Forecast>> getForecasts(String city) {
        String query = city + "," + COUNTRY_CODE;
        return weatherApi.getForecastData(API_KEY, UNIT, query).map(response -> response.forecasts);
    }
}
