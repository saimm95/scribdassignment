package com.example.abc.scribdassignment.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WeatherApiResponse {

    @SerializedName("list") public final List<Forecast> forecasts;

    public WeatherApiResponse(List<Forecast> forecasts) {
        this.forecasts = forecasts;
    }
}
