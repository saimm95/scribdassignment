package com.example.abc.scribdassignment.common;

import android.util.Log;

import com.example.abc.scribdassignment.BuildConfig;

public final class Logger {

    private Logger() {
        // no-op
    }

    public static void d(Class clazz, String msg) {
        if (BuildConfig.DEV_MODE) {
            Log.d(clazz.getCanonicalName(), msg);
        }
    }

    public static void e(Class clazz, Throwable throwable) {
        if (BuildConfig.DEV_MODE) {
            Log.e(clazz.getCanonicalName(), throwable.getMessage(), throwable);
        }
    }
}
