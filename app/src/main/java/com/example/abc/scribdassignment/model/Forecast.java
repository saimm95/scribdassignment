package com.example.abc.scribdassignment.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Forecast {

    @SerializedName("dt") public final long date;
    @SerializedName("main") public final Data data;
    @SerializedName("weather") public final List<Weather> weathers;
    @SerializedName("wind") public final Wind wind;

    public Forecast(long date, Data data, List<Weather> weathers, Wind wind) {
        this.date = date;
        this.data = data;
        this.weathers = weathers;
        this.wind = wind;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Forecast forecast = (Forecast)o;

        return date == forecast.date;
    }

    @Override
    public int hashCode() {
        return (int)(date ^ (date >>> 32));
    }

    public class Data {

        @SerializedName("temp") public final double temperature;
        @SerializedName("feels_like") public final double feelsLike;
        @SerializedName("temp_min") public final double tempMin;
        @SerializedName("temp_max") public final double tempMax;
        @SerializedName("humidity") public final int humidity; // in percent

        public Data(double temperature, double feelsLike, double tempMin, double tempMax,
            int humidity) {
            this.temperature = temperature;
            this.feelsLike = feelsLike;
            this.tempMin = tempMin;
            this.tempMax = tempMax;
            this.humidity = humidity;
        }
    }

    public class Weather {

        @SerializedName("main") public final String heading;
        @SerializedName("description") public final String description;

        public Weather(String heading, String description) {
            this.heading = heading;
            this.description = description;
        }
    }

    public class Wind {

        @SerializedName("speed") public final double speed; // metres/sec
        @SerializedName("deg") public final int deg;

        public Wind(double speed, int deg) {
            this.speed = speed;
            this.deg = deg;
        }
    }
}
