## Instructions
- Tap search in toolbar
- Enter any Canadian city and press 'done'
- App should show the 5-day forecast for the searched city (or an error message if something went wrong)